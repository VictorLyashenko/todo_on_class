'use strict';

let LS = localStorage;
let cards = LS.getItem('cards');

if(cards !== null){
	let parseCards = JSON.parse(cards);
	for(let key in parseCards) {
		let cardObj = parseCards[key];
		cardObj.id = key;

		let card = new Card(cardObj);
		card.init();
	}
}

let createButton = document.querySelector('#createButton');
createButton.addEventListener('click', (event) => {
	event.preventDefault();
	let createForm = document.querySelector('#createForm');

	let cardObj = {};

	let titleField = createForm.querySelector('#title');
	let textField = createForm.querySelector('#text');
	let importantField = createForm.querySelector('#important');

	cardObj.titles = titleField.value;
	cardObj.text = textField.value;
	cardObj.important = importantField.checked;

	titleField.value = '';
	textField.value = '';
	importantField.checked = false;

	let card = new Card(cardObj);
	card.init();
});
